﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class KeyboardBackend : MonoBehaviour {
    public static KeyboardBackend instance;

    public class Node
    {
        public string word;
        public int dist; // levenshtein distance from its parent
        public List<Node> children = new List<Node>(); // the node itself and its distance from the parent

        public Node()
        {
            word = "";
            dist = 0;
        }

        public Node(string w)
        {
            word = w;
            dist = 0;
        }

        public void addChild(Node child)
        {
            children.Add(child);
        }

    }

    Dictionary<char, string > neighbors;
    //char[][] neigh;
    public TextAsset dictionaryFile;
    Node root;
    int maxNumMatches = 40;

	// Use this for initialization
	void Start () {

        instance = this;
        createNeighborList();
        build();

        Debug.Log( "mat and cat: " + levenshteinDistance("mat" , "cat") );
        Debug.Log( "me and you: " + levenshteinDistance("me", "you") );

        Debug.Log("kitten and sitting: " + levenshteinDistance("kitten" , "sitting") );
        Debug.Log("sad and dad: " + levenshteinDistance("sad", "dad"));

        //autoCorrect(1, "CAQE");
        //string s1 = "BOOK";
        //string s2 = "CAQE";

        //Debug.Log("---------s1: " + s1 + " s2: " + s2 + " L Dist: " + levenshteinDistance(s1, s2));
    }

    void createNeighborList()
    {
        neighbors = new Dictionary<char, string >();

        neighbors.Add('1', "2WQ");
        neighbors.Add('2', "3EWQ1");
        neighbors.Add('3', "4REW2");
        neighbors.Add('4', "5TRE3");
        neighbors.Add('5', "6YTR4");
        neighbors.Add('6', "7UYT5");
        neighbors.Add('7', "8IUY6");
        neighbors.Add('8', "9OIU7");
        neighbors.Add('9', "0POI8");
        neighbors.Add('0', "PO9");

        neighbors.Add('Q', "12WA");
        neighbors.Add('W', "23ESAQ1");
        neighbors.Add('E', "34RDSW2");
        neighbors.Add('R', "45TFDE2");
        neighbors.Add('T', "56YGFR4");
        neighbors.Add('Y', "67UHGT5");
        neighbors.Add('U', "78IJHY6");
        neighbors.Add('I', "89OKJU7");
        neighbors.Add('O', "90PLKI8");
        neighbors.Add('P', "0LO9");

        neighbors.Add('A', "QWSZ");
        neighbors.Add('S', "EDXZAW");
        neighbors.Add('D', "RFCXZSE");
        neighbors.Add('F', "TGVCXDR");
        neighbors.Add('G', "YHBVCFT");
        neighbors.Add('H', "UJNBVGY");
        neighbors.Add('J', "IKMNBHU");
        neighbors.Add('K', "OLMNJI");
        neighbors.Add('L', "PMKO");

        neighbors.Add('Z', "SDXA");
        neighbors.Add('X', "DFCZS");
        neighbors.Add('C', "FGVXD");
        neighbors.Add('V', "GHBCF");
        neighbors.Add('B', "HJNVG");
        neighbors.Add('N', "JKMBH");
        neighbors.Add('M', "KLNJ");

    }

    //----------------
    //		Build
    //----------------
    bool build()
    {
        char[] delimiters = {' ', '\n'};
        string doc = dictionaryFile.text;
        string[] words = doc.Split(delimiters);

        //foreach (string s in words)
        for(int i=0; i < words.Length; i++)
        {
            words[i] = words[i].Trim();
            words[i] = words[i].ToUpper();

            if (i == 0)
            {
                root = new Node(words[0]);
            }
            else
            {
                Node child = new Node(words[i]);

                addNode(child);

             }
        }


        return false;
    }

    //----------------
    //	add node
    //----------------
    void addNode(Node child)
	{
		int levDist = levenshteinDistance(root.word, child.word);
		child.dist = levDist;
		bool hasBeenAdded = false;

        // find edge with same distance
        if (root.children.Count > 0)
        {
            foreach (Node currChild in root.children)
            {
                if (currChild.dist == levDist)
                {
                    addNodeRecursive(child, root);
                    hasBeenAdded = true;
                }

            }
        }

        // if none have the same distance, add it as a child
        if (!hasBeenAdded) root.children.Add(child);
    }

    //----------------
    //	add node recursive
    //----------------
    void addNodeRecursive(Node child, Node node)
	{
		bool hasBeenAdded = false;
		int levDist = levenshteinDistance(node.word, child.word);
		child.dist = levDist;

		// find edge with same distance
        foreach (Node currChild in node.children)
        {
			if ( currChild.dist == levDist)
			{
				addNodeRecursive(child, currChild);
				hasBeenAdded = true;
			}
		}

		if(!hasBeenAdded) node.children.Add( child );
	}

    //----------------
    // autocorrect
    //----------------
    public List<string> autoCorrect(int tol, string word)
	{
		int dist = 0;
		List<string> candidateWords = new List<string>();

		Queue< Node> q = new Queue< Node >();
		Node currNode;

        string w = word.ToUpper();

		// tol = tolerance for misspelling errors

		q.Enqueue(root);

		while (q.Count != 0)
		{
			currNode = q.Peek();
            //Debug.Log(currNode.word + " has " + currNode.children.Count + " children");
			//cout << currNode->word << " has " << currNode->children.size() << " children" << endl;

			// compare w/ word to see how far it is
			dist = levenshteinDistance(currNode.word, w);
            //Debug.Log(currNode.word + ", " + w + " DISTANCE: " + dist);
			//cout << currNode->word << ", " << word << " DISTANCE: " << dist << endl;

            // check if word can be added
            if(dist <= tol)
            {
                candidateWords.Add(currNode.word);
            }

			// then check all children betwen dist-tol,dist+tol away
            foreach(Node currChild in currNode.children)
			{
				// ignore any children not within the tolerance
				if (currChild.dist == dist)
				{
                    //cout << "distance for both is " << dist << ", adding " << (*it)->word << " as candidate" << endl;
                    //Debug.Log("distance for both is " + dist + ", adding " + currChild.word + " as a candidate");
					// add word as a candidate
					candidateWords.Add(currChild.word);
				}
				if (currChild.dist >= (dist - tol) && currChild.dist <= (dist + tol))
				{
                    //cout << "distance " << (*it)->dist << " is within tolerance, adding " << (*it)->word << " to queue" << endl;
                    //Debug.Log("distance " + currChild.dist + " is within tolerance, adding " + currChild.word + " to queue");
					q.Enqueue( currChild );
				}
                else
                {
                    //Debug.Log("range: " + (dist - tol) + " to " + (dist + tol) );
                    //Debug.Log("distance " + currChild.dist + " NOT within tolerance, skipping " + currChild.word);
                    //cout << "distance " << (*it)->dist << " NOT within tolerance, skipping " << (*it)->word << endl;
                }
			}

			q.Dequeue();
            if(candidateWords.Count > maxNumMatches) { break; }
		}

        //Debug.Log("candidate words found... " + candidateWords.Count);
        int total = 3;
        if(candidateWords.Count < 3) { total = candidateWords.Count; }

        for (int i = 0; i < total; ++i)
        {
            Debug.Log(candidateWords[i]);
        }

        if (total == 0)
        {
            Debug.LogError("No candidate words found in dictionary!");
        }

        return candidateWords;

    }


    //----------------
    // Levenshtein Distance
    //----------------
    // General steps taken from wikipedia: https://en.wikipedia.org/wiki/Levenshtein_distance
    int levenshteinDistance(string st1, string st2)
	{
		int s1Len = st1.Length;
		int s2Len = st2.Length;

        string upSt1 = st1.ToUpper();
        string upSt2 = st2.ToUpper();

        // create a 2D array that holds the distance between all substrings of the 2 strings
        //List<List<int> > dist = new List<List<int>>( (s1Len + 1)*(s2Len + 1));
        int[][] dist = new int[s1Len + 1][]; // can only define one dimension on initialization

        for(int i=0; i < s1Len + 1; ++i)
        {
            dist[i] = new int[s2Len + 1];
        }

        //s2Len + 1
        //Debug.Log("string 1: " + upSt1 + " string 2: " + upSt2);

        int subCost = 0;

		// set all to zero
		for (int i = 0; i < s1Len + 1; ++i)
		{
			for (int j = 0; j < s2Len + 1; ++j)
			{
				if (i == 0)
				{
					// empty string at i would need j transformations
					dist[i][j] = j*2;
				}
				else if (j == 0)
				{
					// empty string at j would need i transformations
					dist[i][j] = i*2;
				}
				else
				{
					dist[i][j] = 0;
				}
			} // end of inner for-loop
		} // end of outer for-loop

		// go through both strings and find the amount of changes needed
		// to switch between both words
		for (int i = 1; i < s1Len + 1; ++i)
		{
			for (int j = 1; j < s2Len + 1; ++j)
			{
				if (upSt1[i-1] == upSt2[j-1])
				{
					subCost = 0;
				}
                else if ( neighbors[(upSt1[i - 1])].IndexOf(upSt2[j - 1]) != -1 )
                {
                    subCost = 1;
                }
                else
				{
					subCost = 2;
				}

				dist[i][j] = Mathf.Min( Mathf.Min( dist[i-1][j] + 2, // deletion
										dist[i][j-1] + 2 ), // insertion
								dist[i-1][j-1] + subCost); // substitution
			}
		}

		//for (int i = 0; i < s1Len + 1; ++i)
		//{
		//	for (int j = 0; j < s2Len + 1; ++j)
		//	{
		//		cout << dist[i][j] << " ";
		//	}

		//	cout << endl;
		//}

		return dist[s1Len][s2Len];
	}


	// Update is called once per frame
	void Update () {
	
	}
}
